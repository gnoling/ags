#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

readonly pfx="$PWD/local"
mkdir -p "$pfx"

# build Allegro
#
pushd "allegro"
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX="$pfx" ..
make -j "$(nproc)"
make install
popd

export PATH="$pfx/bin:$PATH"
export PKG_CONFIG_PATH="$pfx/lib/pkgconfig"

# build Dynamic Universal Music Bibliotheque
#
pushd "dumb"
mkdir -p build
cd build
cmake \
	-D CMAKE_BUILD_TYPE=Release \
	-D CMAKE_INSTALL_PREFIX="$pfx" \
	-D BUILD_SHARED_LIBS=ON \
	-D BUILD_ALLEGRO4=ON \
	-D BUILD_EXAMPLES=OFF \
	..
make -j "$(nproc)"
make install
popd

# build agsteamstub
#
pushd "agsteamstub"
cp "../source/Engine/plugin/agsplugin.h" .
make -j "$(nproc)"
popd

# build Adventure Game Studio Engine
#
pushd "source"
export CPATH="$pfx/include:$CPATH"
make --directory=Engine -j "$(nproc)"
popd

for app_id in $STEAM_APP_ID_LIST ; do
	make --directory="source/Engine" PREFIX="$PWD/$app_id/dist" install
	mkdir -p "$app_id/dist/lib"
	cp -rf -t "$app_id/dist/lib/" "$pfx/lib/allegro"
	cp -t "$app_id/dist/lib/" "$pfx/lib/"*.so*
	cp -t "$app_id/dist/lib/" "agsteamstub/libagsteam.so"
	cp -t "$app_id/dist/" "ags.sh"
done
